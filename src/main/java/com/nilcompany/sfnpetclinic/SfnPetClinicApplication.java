package com.nilcompany.sfnpetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SfnPetClinicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SfnPetClinicApplication.class, args);
	}

}
